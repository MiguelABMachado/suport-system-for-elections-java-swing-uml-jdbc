/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logNegoc;

import data.EleitoresDAO;
import data.VotantesDAO;

/**
 *
 * @author Jorge
 */
public class EleitoresFacade {

    protected EleitoresDAO eleitores;
    protected VotantesDAO votantes;

    public EleitoresFacade() {
        this.eleitores = new EleitoresDAO();
        this.votantes = new VotantesDAO();
    }

    public boolean remove(String text) {
        Eleitor e=eleitores.remove(Integer.parseInt(text));
        if(e==null){
            return false;
        }
        return true;
    }

}
