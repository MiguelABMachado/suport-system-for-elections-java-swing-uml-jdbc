/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logNegoc;

/**
 *
 * @author rcamposinhos
 */
public class Concelho {

    private int id;
    private String designacao;
    private Circulo circulo;

    public Concelho(String designacao, Circulo circulo) {
        this.designacao = designacao;
        this.circulo = circulo;
    }

    public Concelho(int id, String designacao, Circulo circulo) {
        this.id = id;
        this.designacao = designacao;
        this.circulo = circulo;
    }

    public int getId() {
        return id;
    }

    public Circulo getCirculo() {
        return circulo;
    }

    public String getDesignacao() {
        return designacao;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + this.id;
        return hash;
    }

}
