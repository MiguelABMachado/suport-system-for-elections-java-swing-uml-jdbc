/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logNegoc;

/**
 *
 * @author rcamposinhos
 */
public class Eleitor {

    private int numeroEleitor;
    private String nome;
    private String password;
    private Freguesia freguesia;
    private boolean admin;

    public Eleitor() {
    }

    public Eleitor(int numeroEleitor, String nome, String password, Freguesia freguesia, boolean admin) {
        this.numeroEleitor = numeroEleitor;
        this.nome = nome;
        this.password = password;
        this.freguesia = freguesia;
        this.admin = admin;
    }

    public Eleitor(String nome, String password, Freguesia freguesia, boolean admin) {
        this.numeroEleitor = 0;
        this.nome = nome;
        this.password = password;
        this.freguesia = freguesia;
        this.admin = admin;
    }

    public int getCodigo() {
        return numeroEleitor;
    }

    public Freguesia getFreguesia() {
        return freguesia;
    }

    public String getNome() {
        return nome;
    }

    public int getNumeroEleitor() {
        return numeroEleitor;
    }

    public String getPassword() {
        return password;
    }

    public boolean getAdmin() {
        return this.admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public void setFreguesia(Freguesia freguesia) {
        this.freguesia = freguesia;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setNumeroEleitor(int numeroEleitor) {
        this.numeroEleitor = numeroEleitor;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj); //To change body of generated methods, choose Tools | Templates.
    }

    public Circulo getCirculo() {
        return this.freguesia.getConcelho().getCirculo();
    }

}
