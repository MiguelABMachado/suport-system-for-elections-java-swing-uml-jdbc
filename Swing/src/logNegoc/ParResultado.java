/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logNegoc;

/**
 *
 * @author rcamposinhos
 */
public class ParResultado {

    private Candidatura c;
    private int votosHondt;

    public ParResultado(Candidatura c, int votosHondt) {
        this.c = c;
        this.votosHondt = votosHondt;
    }

    public Candidatura getCandidatura() {
        return c;
    }

    public int getVotosHondt() {
        return votosHondt;
    }

}
