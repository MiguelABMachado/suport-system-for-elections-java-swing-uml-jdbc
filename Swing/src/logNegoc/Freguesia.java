/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logNegoc;

/**
 *
 * @author rcamposinhos
 */
public class Freguesia {

    private int id;
    private String designacao;
    private Concelho concelho;

    public Freguesia(String designacao, Concelho concelho) {
        this.designacao = designacao;
        this.concelho = concelho;
    }

    public Freguesia(int id, String designacao, Concelho concelho) {
        this.id = id;
        this.designacao = designacao;
        this.concelho = concelho;
    }

    public int getId() {
        return id;
    }

    public Concelho getConcelho() {
        return concelho;
    }

    public String getDesignacao() {
        return designacao;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj); //To change body of generated methods, choose Tools | Templates.
    }

}
