/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logNegoc;

import java.util.ArrayList;
import java.util.List;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

/**
 *
 * @author rcamposinhos
 */
public class SGEleicoes {

    public EleitoresFacade eleitores;
    public CandidaturasFacade candidaturas;
    public EleicoesFacade eleicoes;
    public LocaisFacade locais;

    private Eleitor eleitor; //-> Eleitor que faz log in
    private int tipo;

    public SGEleicoes() {
        this.eleitores = new EleitoresFacade();
        this.eleicoes = new EleicoesFacade();
        this.candidaturas = new CandidaturasFacade();
        this.locais = new LocaisFacade();
        this.tipo = 0;
    }

    public SGEleicoes(EleitoresFacade eleitores, CandidaturasFacade candidaturas, EleicoesFacade eleicoes, LocaisFacade locais, Eleitor eleitor, int tipo) {
        this.eleitores = eleitores;
        this.candidaturas = candidaturas;
        this.eleicoes = eleicoes;
        this.locais = locais;
        this.eleitor = eleitor;
        this.tipo = tipo;
    }

    public List<Object[]> getEleicoesByEleitor(String codigo) {
        int codEleitor = Integer.parseInt(codigo);
        List<Votante> vot = new ArrayList<>(this.eleitores.votantes.get(codEleitor));
        List<Eleicao> aux = new ArrayList<>(this.eleicoes.eleicoes.values());
        //filtar por data
        aux = eleicoes.filterEleicoes(eleitor.getFreguesia().getConcelho().getCirculo().getId(), aux);
        //filtar por já votado
        Iterator<Eleicao> iter = aux.iterator();
        while (iter.hasNext()) {
            Eleicao e2 = iter.next();
            for (Votante v : vot) {
                Eleicao e = v.getEleicao();
                if (e.equals(e2)) {
                    iter.remove();
                }
            }
        }
        ArrayList<Object[]> o = new ArrayList<>();
        for (Eleicao e : aux) {
            o.add(new Object[]{e.getDesignacao(), e.getDataFim().toString().replace('T', ' '), e.getId()});
        }

        return o;

    }

    public void criaEleicao(String nome, int tipo, LocalDateTime di, LocalDateTime df, ArrayList<Object[]> circulos) {

        ArrayList<Circulo> aux = new ArrayList<>();

        for (Object[] o : circulos) {
            Circulo c = new Circulo((int) o[0], (String) o[1], (int) o[2]);
            aux.add(c);
        }

        Eleicao e = new Eleicao(nome, di, df, aux, tipo);

        this.eleicoes.eleicoes.put(e);
    }

    public void criaCandidatura(String nome, int idEleicao, String logotipo, ArrayList<Object[]> candidatos) {
        ArrayList<Candidato> cands = new ArrayList<>();

        for (Object[] o : candidatos) {
            Circulo circ = this.locais.getIdCirculoByName((String) o[1]);
            Candidato aux = new Candidato((String) o[0], circ, (int) o[2]);

            cands.add(aux);
        }

        Eleicao e = eleicoes.eleicoes.get(idEleicao);
        Candidatura c = new Candidatura(nome, logotipo, e, cands);

        this.candidaturas.candidaturas.put(c);
    }

    public Eleicao selectEleicao(List<Eleicao> lista, String eleicao) {
        for (Eleicao e : lista) {
            if (e.getDesignacao().equals(eleicao)) {
                return e;
            }
        }
        return null;
    }

    public List<String[]> getCandByElei(int elei) {
        Eleicao e = eleicoes.eleicoes.get(elei);
        if (e.getTipo() == 2) {
            return candidaturas.getCandByElei(elei);
        } else {
            return candidaturas.getCandByElei(elei, eleitor.getFreguesia().getConcelho().getCirculo().getId());
        }
    }

    public boolean login(int username, String password, boolean adminCheck) {
        Eleitor aux = this.eleitores.eleitores.get(username);

        boolean flag = false;
        if (aux != null) {
            if (adminCheck) {
                if (aux.getAdmin() == adminCheck) {
                    flag = aux.getPassword().equals(password);
                } else {
                    return false;
                }
            }
            flag = aux.getPassword().equals(password);
        }
        if (flag) {
            this.eleitor = aux;
        }
        return flag;
    }

    public boolean registarVoto(int candidatura, int cod_elei) {
        return candidaturas.candidaturas.registaVoto(candidatura, eleitor.getCodigo(), eleitor.getFreguesia().getId(), cod_elei);
    }

    public int abstencaoGlobal(int codEleicao) {
        int nVotos = 0;
        int nEleitores = 0;

        Eleicao el = eleicoes.eleicoes.get(codEleicao);

        nVotos = el.totalVotos();

        for (Eleitor e : eleitores.eleitores.values()) {
            for (Circulo c : el.getCirculos()) {
                if (e.getCirculo().getId() == c.getId()) {
                    nEleitores++;
                }
            }
        }

        return (nEleitores - nVotos);

    }

    public List<Object[]> resultadosGlobais(int codEleicao) {
        ArrayList<Object[]> res = new ArrayList<>();
        Eleicao el = eleicoes.eleicoes.get(codEleicao);
        int nEleitores = 0;
        int nVotos = el.totalVotos();
        //Calcular a abstenção
        for (Eleitor e : eleitores.eleitores.values()) {
            for (Circulo c : el.getCirculos()) {
                if (e.getCirculo().getId() == c.getId()) {
                    nEleitores++;
                }
            }
        }
        int abstencaoNumero = nEleitores - nVotos;
        double abstencaoPer = (double) abstencaoNumero / nEleitores * 100;
        abstencaoPer = Math.floor(abstencaoPer * 100) / 100;
        List<Object[]> aux = el.resultados();
        for (Object[] o : aux) {
            //o->designacao,votos,mandatos
            double percentagem = (double) Integer.parseInt(o[1].toString()) / nVotos * 100;
            percentagem = Math.floor(percentagem * 100) / 100;
            res.add(new Object[]{o[0], percentagem, o[1], o[2], abstencaoNumero, abstencaoPer});
        }
        return res;
    }

    public List<Object[]> resultadosbyCirculo(int codEleicao, int codCirculo) {
        ArrayList<Object[]> res = new ArrayList<>();
        Eleicao el = eleicoes.eleicoes.get(codEleicao);
        int nEleitores = 0;
        //Buscar o circulo desejado
        Circulo cir = locais.circulos.get(codCirculo);
        int nVotos = el.totalVotosbyCirculo(cir);
        //Calcular abstençao
        for (Eleitor e : eleitores.eleitores.values()) {
            if (e.getCirculo().getId() == cir.getId()) {
                nEleitores++;
            }
        }
        int abstencaoNumero = nEleitores - nVotos;
        double abstencaoPer = (double) abstencaoNumero / nEleitores * 100;
        abstencaoPer = Math.floor(abstencaoPer * 100) / 100;
        List<Object[]> aux = el.resultadosByCirculo(cir);
        for (Object[] o : aux) {
            //o->designacao,votos,mandatos
            double percentagem = (double) Integer.parseInt(o[1].toString()) / nVotos * 100;
            percentagem = Math.floor(percentagem * 100) / 100;
            res.add(new Object[]{o[0], percentagem, o[1], o[2], abstencaoNumero, abstencaoPer});

        }
        return res;
    }

    public List<Object[]> resultadosbyConcelho(int idEleicao, int codCon) {
        ArrayList<Object[]> res = new ArrayList<>();
        Eleicao el = eleicoes.eleicoes.get(idEleicao);
        int nEleitores = 0;
        //Buscar o concelho desejado
        Concelho con = locais.concelhos.get(codCon);
        int nVotos = el.totalVotosbyConcelho(con);
        //Calcular abstençao
        for (Eleitor e : eleitores.eleitores.values()) {
            if (e.getFreguesia().getConcelho().getId() == con.getId()) {
                nEleitores++;
            }
        }
        int abstencaoNumero = nEleitores - nVotos;
        double abstencaoPer = (double) abstencaoNumero / nEleitores * 100;
        abstencaoPer = Math.floor(abstencaoPer * 100) / 100;
        List<Object[]> aux = el.resultadosByConcelho(con);
        for (Object[] o : aux) {
            //o->designacao,votos,mandatos
            double percentagem = (double) Integer.parseInt(o[1].toString()) / nVotos * 100;
            percentagem = Math.floor(percentagem * 100) / 100;
            res.add(new Object[]{o[0], percentagem, o[1], o[2], abstencaoNumero, abstencaoPer});
        }
        return res;
    }

    public List<Object[]> resultadosbyFreguesia(int idEleicao, int codFreg) {
        ArrayList<Object[]> res = new ArrayList<>();
        Eleicao el = eleicoes.eleicoes.get(idEleicao);
        int nEleitores = 0;
        //Buscar a freguesia desejada
        Freguesia freg = locais.freguesias.get(codFreg);
        int nVotos = el.totalVotosbyFreg(freg);
        //Calcular abstençao
        for (Eleitor e : eleitores.eleitores.values()) {
            if (e.getFreguesia().getId() == freg.getId()) {
                nEleitores++;
            }
        }
        int abstencaoNumero = nEleitores - nVotos;
        double abstencaoPer = (double) abstencaoNumero / nEleitores * 100;
        abstencaoPer = Math.floor(abstencaoPer * 100) / 100;
        List<Object[]> aux = el.resultadosByFreguesia(freg);
        for (Object[] o : aux) {
            //o->designacao,votos,mandatos
            double percentagem = (double) Integer.parseInt(o[1].toString()) / nVotos * 100;
            percentagem = Math.floor(percentagem * 100) / 100;
            res.add(new Object[]{o[0], percentagem, o[1], o[2], abstencaoNumero, abstencaoPer});
        }
        return res;
    }

    public String[] getFreguesiasByConcelhoWithoutEleitores(int idConcelho) {
        Concelho c = locais.concelhos.get(idConcelho);
        HashSet<String> aux = this.getFreguesiasComEleitoresByConcelho(idConcelho);
        Collection<Freguesia> freguesias = locais.getFreguesiasByConcelho2(idConcelho);
        String[] res = new String[freguesias.size()];
        int index = 0;
        for (Freguesia f : freguesias) {
            if (!(aux.contains(f.getDesignacao())) && (f.getConcelho().getId() == idConcelho)) {
                res[index] = f.getDesignacao();
                index++;
            }
        }
        String[] res2 = new String[index];
        System.arraycopy(res, 0, res2, 0, index);
        return res2;

    }

    private HashSet<String> getFreguesiasComEleitoresByConcelho(int idConcelho) {
        HashSet<String> res = new HashSet<>();
        ArrayList<Eleitor> aux = new ArrayList<>(this.eleitores.eleitores.values());
        for (Eleitor c : aux) {
            if (c.getFreguesia().getConcelho().getId() == idConcelho) {
                res.add(c.getFreguesia().getDesignacao());
            }
        }
        return res;
    }

    public int criarEleitor(String nome, String password, int freg, boolean eAdmin) {
        Freguesia f = locais.freguesias.get(freg);
        Eleitor e = new Eleitor(nome, password, f, eAdmin);
        e = eleitores.eleitores.put(e);
        return e.getCodigo();
    }

    public boolean updateEleitor(String codigo, String nome, String password, int freg) {
        Freguesia f = locais.freguesias.get(freg);
        Eleitor e = eleitores.eleitores.get(Integer.parseInt(codigo));
        if (e == null) {
            return false;
        }
        e.setFreguesia(f);
        e.setNome(nome);
        e.setPassword(password);
        return eleitores.eleitores.update(e);
    }

}
