/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import logNegoc.Eleicao;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import logNegoc.Circulo;

/**
 *
 * @author Toshiba
 */
public class EleicoesDAO implements Map<Integer,Eleicao>{

    
    @Override
    public int size() {
        int size = -1;
        Connection con = null;
        try {
            con = Connect.connect();
            PreparedStatement ps = con.prepareStatement("select count(idCandidatura) from candidatura");
            ResultSet rs = ps.executeQuery();
            if(rs.next()) {
                size = rs.getInt(1);
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return size;
    }

    @Override
    public boolean isEmpty() {
        int size = this.size();
        return size==0;
    }

    @Override
    public boolean containsKey(Object key) {
        Eleicao e=this.get(Integer.parseInt(key.toString()));
        return e != null;
    }

    @Override
    public Eleicao get(Object key) {
        Eleicao e = null;
        Connection con = null;
        CandidaturasDAO canDAO=new CandidaturasDAO();
        CirculosDAO circDAO=new CirculosDAO();
        try {
            con = Connect.connect();
            PreparedStatement ps = con.prepareStatement("select * from eleicao where idEleicao= ?");
            ps.setInt(1, Integer.parseInt(key.toString()));
            ResultSet rs = ps.executeQuery();
            if(rs.next()) {
                ArrayList<Circulo> all_circ=new ArrayList<>();
                PreparedStatement ps3 = con.prepareStatement("select circulo from eleicaocirculo where eleicao= ?");
                ps3.setInt(1, Integer.parseInt(key.toString()));
                ResultSet rs3 = ps3.executeQuery();
                //vai buscar todos os circulos da eleicao
                while(rs3.next()) {
                    Circulo f=circDAO.getWithMandatos(rs3.getInt("Circulo"),Integer.parseInt(key.toString()));
                    all_circ.add(f);
                }
                LocalDateTime ti = LocalDateTime.parse(rs.getString("DataInicio").replace(' ', 'T'), DateTimeFormatter.ISO_DATE_TIME);
                LocalDateTime tf = LocalDateTime.parse(rs.getString("DataFim").replace(' ', 'T'), DateTimeFormatter.ISO_DATE_TIME);
                e = new Eleicao(rs.getInt("ideleicao"),rs.getString("Designacao"), ti, tf,all_circ,rs.getBoolean("estafechada"),rs.getInt("Tipo"));
            }
        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
        } finally {
            try {
            con.close();
            } catch (Exception ex) {
            ex.printStackTrace();
            }
        }
        return e;
    }
    
    
    @Override
    public Eleicao put(Integer key, Eleicao value) {
        return this.put(value);
    }
    
    public Eleicao put(Eleicao value) {
    Connection con = null;
        int aux=-1;
        try {
            con = Connect.connect();
            con.setAutoCommit(false);
            PreparedStatement ps = con.prepareStatement("insert into eleicao (designacao,dataInicio,datafim,tipo,estafechada) values(?,?,?,?,?)",Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, value.getDesignacao());
            ps.setString(2, value.getDataInicio().format(DateTimeFormatter.ISO_DATE_TIME));
            ps.setString(3, value.getDataFim().format(DateTimeFormatter.ISO_DATE_TIME));
            ps.setInt(4, value.getTipo());
            ps.setBoolean(5,value.isEstaFechada());
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            //Vai buscar o id dessa eleição
            int id_eleicao=-1;
            if(rs.next()) {
                id_eleicao=rs.getInt(1);
            }
            //Vai buscar as chaves dos circulos e inserir em eleicaocirculo
            for(Circulo aux2 : value.getCirculos()){
                    //Vai buscar o id do circulo do candidato
                        PreparedStatement ps2 = con.prepareStatement("insert into eleicaocirculo(eleicao,circulo,NrMandatos) values (?,?,?)");
                        ps2.setInt(1, id_eleicao);
                        ps2.setInt(2, aux2.getId());
                        ps2.setInt(3, aux2.getMandatos());
                        ps2.executeUpdate();
                    }
            con.commit();
        }catch (SQLException | ClassNotFoundException ex) {
            try {
                con.rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(EleicoesDAO.class.getName()).log(Level.SEVERE, null, ex1);
            }
            ex.printStackTrace();
        } finally {
            try {
            con.close();
            } catch (Exception ex) {
            ex.printStackTrace();
            }
        }
        return value;
    }

    @Override
    public Eleicao remove(Object key) {
        Connection con = null;
        Eleicao rem=null;
        try {
            con = Connect.connect();
            rem=this.get(key.toString());
            PreparedStatement ps = con.prepareStatement("delete from eleicaocirculo where Eleicao=?");
            ps.setInt(1, Integer.parseInt(key.toString()));
            ps.executeUpdate();
            ps = con.prepareStatement("delete from eleitoreleicao where Eleicao=?");
            ps.setInt(1, Integer.parseInt(key.toString()));
            ps.executeUpdate();
            ps = con.prepareStatement("delete from candidato where Candidatura in ("
                    + "select idCandidatura from candidatura where eleicao = ?)");
            ps.setInt(1, Integer.parseInt(key.toString()));
            ps.executeUpdate();
            ps = con.prepareStatement("delete from candidaturafreguesia where Candidatura in ("
                    + "select idCandidatura from candidatura where eleicao = ?)");
            ps.setInt(1, Integer.parseInt(key.toString()));
            ps.executeUpdate();
            ps = con.prepareStatement("delete from candidatura where Eleicao=?");
            ps.setInt(1, Integer.parseInt(key.toString()));
            ps.executeUpdate();
            ps = con.prepareStatement("delete from eleicao where idEleicao=?");
            ps.setInt(1, Integer.parseInt(key.toString()));
            ps.executeUpdate();
        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
        } finally {
            try {
            con.close();
            } catch (Exception ex) {
            ex.printStackTrace();
            }
        }
        return rem;
    }
    
    @Override
    public Collection<Eleicao> values() {
        Collection<Eleicao> res = new ArrayList<>();
        Eleicao e = null;
        CandidaturasDAO canDAO=new CandidaturasDAO();
        CirculosDAO circDAO=new CirculosDAO();
        Connection con = null;
        try {
            con = Connect.connect();
            PreparedStatement ps = con.prepareStatement("select * from eleicao");
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                ArrayList<Circulo> all_circ=new ArrayList<>();
                PreparedStatement ps3 = con.prepareStatement("select circulo from eleicaocirculo where eleicao= ?");
                ps3.setInt(1, rs.getInt("idEleicao"));
                ResultSet rs3 = ps3.executeQuery();
                //vai buscar todos os circulos da eleicao
                while(rs3.next()) {
                    Circulo f=circDAO.getWithMandatos(rs3.getInt("Circulo"),rs.getInt("idEleicao"));
                    all_circ.add(f);
                }
                LocalDateTime ti = LocalDateTime.parse(rs.getString("DataInicio").replace(' ', 'T'), DateTimeFormatter.ISO_DATE_TIME);
                LocalDateTime tf = LocalDateTime.parse(rs.getString("DataFim").replace(' ', 'T'), DateTimeFormatter.ISO_DATE_TIME);
                e = new Eleicao(rs.getInt("ideleicao"),rs.getString("Designacao"), ti, tf,all_circ,rs.getBoolean("estafechada"),rs.getInt("Tipo"));
                res.add(e);
            }
        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return res; 
    }
    
    @Override
    public boolean containsValue(Object value) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public Set<Entry<Integer, Eleicao>> entrySet() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public void putAll(Map<? extends Integer, ? extends Eleicao> m) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Set<Integer> keySet() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public boolean fechar(int id) {
        Connection con = null;
        int aux = -1;
        boolean realizado=true;
        try {
            con = Connect.connect();
            PreparedStatement ps = con.prepareStatement("update eleicao set estafechada=true where ideleicao=?");
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException | ClassNotFoundException ex) {
                realizado= false;
        } finally {
            try {
                con.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return realizado;
    }
   }

    



